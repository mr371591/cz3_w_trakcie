function dodaj_do_tabeli_wyniki_kandydatow(db_wyniki, db_kandydaci)
{
    alert("wszedlem do funkcji dodaj_do_tabeli_wyniki_kandydatow");
    wyniki = document.getElementById("tabela_wyniki_kandydatow");
    tr1 = wyniki.appendChild(document.createElement("tr"));
    tr1.className = "wiersz_glowny";

    td1_1 = tr1.appendChild(document.createElement("td"));
    td1_1.className = "kol1";
    td1_1.appendChild(document.createTextNode("Kandydat"));

    td1_2 = tr1.appendChild(document.createElement("td"));
    td1_2.className = "kol2";
    td1_2.appendChild(document.createTextNode("Liczba głosów oddanych na kandydata"));

    td1_3 = tr1.appendChild(document.createElement("td"));
    td1_3.className = "kol3";
    td1_3.appendChild(document.createTextNode("Procentowa liczba głosów"));

    if (typ_statystyk == "")
    {
        tr2 = wyniki.appendChild(document.createElement("tr"));
        tr2.className = "wiersz2";
        td2_1 = tr2.appendChild(document.createElement("td"));
        td2_1.setAttribute("colspan", 2);
        a2_1 = td2_1.appendChild(document.createElement("a"));
        a2_1.setAttribute("href", "{% url 'edit' wojewodztwo=wyniki_szczegolowe.0 okreg=wyniki_szczegolowe.1 powiat=wyniki_szczegolowe.2 gmina=wyniki_szczegolowe.3 %}");
        a2_1.appendChild(document.createTextNode("EDYTUJ"));
    }
    for (var x in data.dane)
    {
        var tr_wiersz1 = wyniki.appendChild(document.createElement("tr"));
        tr_wiersz1.className = "wiersz1";

        var td_wiersz1_1 = tr_wiersz1.appendChild(document.createElement("td"));
        td_wiersz1_1.className = "kol1";
        td_wiersz1_1.appendChild(document.createTextNode(x.parzyste.kandydat));

        var td_wiersz1_2 = tr_wiersz1.appendChild(document.createElement("td"));
        td_wiersz1_2.className = "kol2";
        td_wiersz1_2.appendChild(document.createTextNode(x.parzyste.glosy));

        var td_wiersz1_2 = tr_wiersz1.appendChild(document.createElement("td"));
        td_wiersz1_3.className = "kol3";
        td_wiersz1_3.appendChild(document.createTextNode(x.parzyste.procent + "%"));


        var tr_wiersz2 = wyniki.appendChild(document.createElement("tr"));
        tr_wiersz2.className = "wiersz2";

        var td_wiersz2_1 = tr_wiersz2.appendChild(document.createElement("td"));
        td_wiersz2_1.className = "kol1";
        td_wiersz2_1.appendChild(document.createTextNode(x.nieparzyste.kandydat));

        var td_wiersz2_2 = tr_wiersz2.appendChild(document.createElement("td"));
        td_wiersz2_2.className = "kol2";
        td_wiersz2_2.appendChild(document.createTextNode(x.nieparzyste.glosy));

        var td_wiersz2_2 = tr_wiersz2.appendChild(document.createElement("td"));
        td_wiersz2_3.className = "kol3";
        td_wiersz2_3.appendChild(document.createTextNode(x.nieparzyste.procent + "%"));
    }
}

function dodaj_do_tabeli_statystyki_tabela(data)
{
    var tab = document.getElementById("statysytki_tabela");

    var dane_do_kol1 = [
        "Liczba uprawnionych do glosowania",
        "Liczba wydanych kart do glosowania",
        "Liczba kart wyjetych z urny",
        "Liczba ważnych głosow",
        "Liczba niewaznych glosow",
        "Frekwencja"
    ];
    var dane_do_kol2 = [
        statystyki_obszaru.uprawnionych.toString(),
        statystyki_obszaru.wydanych_kart.toString(),
        statystyki_obszaru.wyjetych_kart.toString(),
        statystyki_obszaru.waznych_glosow.toString(),
        statystyki_obszaru.niewaznych_glosow.toString(),
        frekwencja.toString() + "%"
    ];
    var tab_wierszy = [];
    var tab_kol1 = [];
    var tab_kol2 = [];
    for(var x = 0; x < 6; x++)
    {
        tab_wierszy[x] = tab.appendChild(document.createElement("tr"));
        if(x % 2 == 0) tab_wierszy[x].className = "wiersz1";
        else tab_wierszy[x].className = "wiersz2";

        tab_kol1[x] = tab_wierszy[x].appendChild(document.createElement("td"));
        tab_kol1[x].className = "kol1";
        tab_kol1[x].appendChild(document.createTextNode(dane_do_kol1[x]));

        tab_kol2[x] = tab_wierszy[x].appendChild(document.createElement("td"));
        tab_kol2[x].className = "kol2";
        tab_kol2[x].appendChild(document.createTextNode(dane_do_kol2[x]));
    }
}

function dodaj_do_tabeli_wyniki(data)
{
    var tab = document.getElementById("tabela_wyniki");

    var tr1 = tab.appendChild(document.createElement("tr"));
    tr1.className = "wiersz2";

    var tr1_nazwy = [
        typ_statystyk,
        "Liczba uprawnionych do głosowania",
        "Liczba wydanych kart do glosowania",
        "Liczba kart wyjetych z urny",
        "Liczba ważnych glosow",
        "Liczba niewaznych glosow",
        "Frekfencja"
    ];
    var tab_kolumn1 = [];

    for(var x = 0; x < 7; x++)
    {
        tab_kolumn1[x] = tr1.appendChild(document.createElement("td"));
        tab_kolumn1[x].createTextNode(tr1_nazwy[x]);
    }

    var licznik = 0;

    var tab_wierszy = [];
    for(var wynik in wyniki_szczegolowe)
    {
        tab_wierszy[licznik] = tab.appendChild(document.createElement("tr"));
        if(licznik % 2 == 0) tab_wierszy[licznik].className = "wiersz2";
        else tab_wierszy[licznik].className = "wiersz1";
        var tab_kolumn = [];
        tab_kolumn[0] = tab_wierszy[licznik].appendChild(document.createElement("td")).appendChild(document.createElement("a"));
        if(typ_statystyk == "wojewodztwo")
        {
            tab_kolumn[0].setAttribute("href", "{% url 'woj' wojewodztwo=wynik.szczegoly.wojewodztwo %}");
            tab_kolumn[0].createTextNode(wynik.szczegoly.wojewodztwo);
        }
        else if(typ_statystyk == "okreg")
        {
            tab_kolumn[0].setAttribute("href", "{% url 'okr' wojewodztwo=wynik.szczegoly.wojewodztwo okreg=wynik.szczegoly.okreg %}");
            tab_kolumn[0].createTextNode(wynik.szczegoly.okreg);
        }
        else if(typ_statystyk == "powiat")
        {
            tab_kolumn[0].setAttribute("href", "{% url 'pow' wojewodztwo=wynik.szczegoly.wojewodztwo okreg=wynik.szczegoly.okreg powiat=wynik.powiat_z_podkreslnikiem %}");
            tab_kolumn[0].createTextNode(wynik.szczegoly.powiat);
        }
        else if(typ_statystyk == "gmina")
        {
            tab_kolumn[0].setAttribute("href", "{% url 'gmi' wojewodztwo=wynik.szczegoly.wojewodztwo okreg=wynik.szczegoly.okreg powiat=wynik.powiat_z_podkreslnikiem gmina=wynik.gmina_z_podkreslnikiem %}");
            tab_kolumn[0].createTextNode(wynik.szczegoly.gmina);
        }
        licznik_wiersza++;
        var pozostale_dane = [];
        pozostale_dane[0] = wynik.szczegoly.uprawnionych;
        pozostale_dane[1] = wynik.szczegoly.wydanych_kart;
        pozostale_dane[2] = wynik.szczegoly.wyjetych_kart;
        pozostale_dane[3] = wynik.szczegoly.waznych_glosow;
        pozostale_dane[4] = wynik.szczegoly.niewaznych_glosow;
        pozostale_dane[5] = wynik.frekwencja.toString() + "%";
        var wiersze_pozostalych_danych = [];
        for(var x = 0; x < 6; x++)
        {
            wiersze_pozostalych_danych[x] = tab_wierszy[licznik].appendChild(document.createElement("td"));
            wiersze_pozostalych_danych[x].createTextNode(pozostale_dane[x])
        }
        licznik++;
    }
}

function displayWyniki(jsonWyniki, jsonKandydaci)
{
    var wyniki = JSON.parse(jsonWyniki);
    var kandydaci = JSON.parse(jsonKandydaci);

    dodaj_do_tabeli_wyniki_kandydatow(wyniki, kandydaci);
    dodaj_do_tabeli_statystyki_tabela(wyniki, kandydaci);
    dodaj_do_tabeli_wyniki(wyniki, kandydaci);
    // alert("Doszedlem tutaj");
    // var tab = document.getElementById("tabela_wyniki_kandydatow");
    // tab.setAttribute("abc", "123");
    // alert("Skończyłem dodawać atrybut");
}

function displayQuestions(jsonQuestions) {
    var data = JSON.parse(jsonQuestions);

    var questions = document.getElementById("questions");

    while (questions.firstChild) {
        questions.removeChild(questions.firstChild);
    }

    for (var ix in data) {
        var li = questions.appendChild(document.createElement("li"));
        li.appendChild(document.createTextNode(data[ix]["question"] + " "));

        var delLink = li.appendChild(document.createElement("a"));
        delLink.appendChild(document.createTextNode("[x]"));
        delLink.questionId = data[ix]["pk"];
        delLink.onclick = function() {
            var req = new XMLHttpRequest();
            req.open("DELETE", "http://localhost:8000/rest/delete/" + this.questionId + "/");
            req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            req.addEventListener("error", function() {
                alert("Error: " + this.responseText);
            });
            req.addEventListener("load", function() {
                alert("OK");
            });
            req.send();
        };
    }
}

function refresh() {
    //JESZCZE NIE WIEM CO ZROBIĆ ABY PODAĆ DO "displayWyniki" dane z req_lista_kandydatow i req_lista_wynikow
    // UPDATE : JESZCZE nie wiem czy robię to dobrze !!!
    var req_lista_Kandydatow = new XMLHttpRequest();
    req_lista_Kandydatow.open("GET", "http://localhost:8000/rest/listaKandydatow/");
    req_lista_Kandydatow.addEventListener("error", function() {
        alert("Error: " + this.responseText);
        document.getElementById("status").firstChild.textContent = "offline";
    });

    var req_lista_Wynikow = new XMLHttpRequest();
    req_lista_Wynikow.open("GET", "http://localhost:8000/rest/listaWynikow/");
    req_lista_Wynikow.addEventListener("error", function() {
        alert("Error: " + this.responseText);
        document.getElementById("status").firstChild.textContent = "offline";
    });

    req_lista_Kandydatow.addEventListener("load", function() {
        displayWyniki(this.responseText);

        localStorage.setItem("Kandydaci", this.responseText);
        localStorage.setItem("Wyniki", req_lista_Wynikow.responseText);
        document.getElementById("status").firstChild.textContent = "online";
    });
    req_lista_Kandydatow.send();


    req_lista_Wynikow.addEventListener("load", function() {
        displayWyniki(this.responseText);

        localStorage.setItem("Wyniki", this.responseText);
        localStorage.setItem("Kandydaci", req_lista_Kandydatow.responseText);
        document.getElementById("status").firstChild.textContent = "online";
    });
    req_lista_Wynikow.send();
}

function init() {
    // alert("jestem tutaj");
    tabelaWyniki = localStorage.getItem("Wyniki");
    tabelaKandydaci = localStorage.getItem("Kandydaci");
    // alert("a teraz tutaj");
    // var previousQuestions = localStorage.getItem("questions");
    if (tabelaWyniki != null && tabelaKandydaci != null) {
        // alert("tabela nie jest nullem");
        displayWyniki(tabelaWyniki, tabelaKandydaci);
        // displayQuestions(previousQuestions);
    }
    refresh();
}