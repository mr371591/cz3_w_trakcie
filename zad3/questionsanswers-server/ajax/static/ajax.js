function sendAJAXQuestion() {
    var question = document.getElementById("question").value;

    req = new XMLHttpRequest();
    req.addEventListener("load", function() {
        if (this.responseText != "OK") {
            alert("AJAX error!");
        }
    });
    req.open("GET", "/ajax/ask/?question=" + question);
    req.send();
}

function refreshQuestions() {
    req = new XMLHttpRequest();
    req.addEventListener("load", function() {
        var questions = JSON.parse(this.responseText);
        var list = document.getElementById("list");
        for (var i = list.children.length - 1; i >= 0; --i) {
            list.removeChild(list.children[i]);
        }
        for (var i = 0; i < questions.length; ++i) {
            list.appendChild(document.createElement("li")).appendChild(document.createTextNode(questions[i]));
        }
    });
    req.open("GET", "/ajax/list/");
    req.send();

    setTimeout(refreshQuestions, 1000);
}

window.onload = refreshQuestions();