from rest_framework.serializers import ModelSerializer
from ajax.models import WynikZPost, Wynik, Kandydat

class WynikZPostFormSerializer(ModelSerializer):

    class Meta:
        model = WynikZPost
        kandydaci = Kandydat.objects.all()
        fields = ('w1', 'w2', 'w3', 'w4', 'w5', 'w6', 'w7', 'w8', 'w9', 'w10', 'w11', 'w12')


class WynikSerializer(ModelSerializer):
    class Meta:
        model = Wynik
        fields = (
            'wojewodztwo', 'okreg', 'powiat', 'gmina',
            'w1', 'w2', 'w3', 'w4', 'w5', 'w6', 'w7', 'w8', 'w9', 'w10', 'w11', 'w12',
            'uprawnionych', 'wydanych_kart', 'wyjetych_kart', 'niewaznych_glosow', 'waznych_glosow'
        )


class KandydatSerializer(ModelSerializer):
    class Meta:
        model = Kandydat
        fields = ('numerKandydata', 'imieINazwisko')
