"""questionsanswers URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
import qa.views
import ajax.views
import rest.views

urlpatterns = [
    url(
        r'^$',
        qa.views.logowanie,
        {},
        name='logowanie'
    ),
    url(
        r'^polska$',
        qa.views.strona,
        {'wojewodztwo': '', 'okreg': '', 'powiat': '', 'gmina': ''},
        name='polska'
    ),
    url(
        r'^wyszukiwarka$',
        qa.views.wyszukiwarka,
        {},
        name='wyszukiwarka'
    ),
    url(
        r'^polska/wojewodztwo/(?P<wojewodztwo>[\w-]+)$',
        qa.views.strona,
        {'okreg': '', 'powiat': '', 'gmina': ''},
        name='woj'
    ),
    url(
        r'^polska/wojewodztwo/(?P<wojewodztwo>[\w-]+)/okreg/(?P<okreg>[\w-]+)$',
        qa.views.strona,
        {'powiat': '', 'gmina': ''},
        name='okr'),
    url(
        r'^polska/wojewodztwo/(?P<wojewodztwo>[\w-]+)/okreg/(?P<okreg>[\w-]+)/powiat/(?P<powiat>[>_\w-]+)$',
        qa.views.strona,
        {'gmina': ''},
        name='pow'
    ),
    url(
        r'^polska/wojewodztwo/(?P<wojewodztwo>[\w-]+)/okreg/(?P<okreg>[\w-]+)/powiat/(?P<powiat>[._\w-]+)/gmina/(?P<gmina>[._\w-]+)$',
        qa.views.strona,
        name='gmi'
    ),
    url(
        r'^polska/wojewodztwo/(?P<wojewodztwo>[\w-]+)/okreg/(?P<okreg>[\w-]+)/powiat/(?P<powiat>[._\w-]+)/gmina/(?P<gmina>[._\w-]+)/edycja$',
        qa.views.edycja,
        name='edit'
    ),
    url(
        r'^rest/listaKandydatow/$', rest.views.listKandydaci
    ),
    url(
        r'^rest/listaWynikow/$', rest.views.listWyniki
    )
]